class myClass:
    a='Class Attribute'
    b='Another Attribute'

    def intro(self,name,color):
        return 'Color of {} is {}'.format(name,color)

ob=myClass()
# print(ob)           # Returns Memory Location
# print(ob.a)

ob1=myClass()
# print(ob1.b)
# print(ob1.__class__.a)      # Another way to access attributes of a class

print(ob.intro('Parrot','Green'))
print(ob1.intro('Cow','White'))