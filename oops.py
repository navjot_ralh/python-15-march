class circle:
    # Class Attribute
    p=3.14
    # Class Method
    def area(self,r):
        return self.p*r*r

    def param(self,r):
        return 2*self.p*r

# Creating Object
ob1=circle()

# Access Attributes of a class
# print(ob1.p) 

# Access Methods of a class
# print(ob1.area(10))

# Input from User
radius=int(input('Enter Radius of circle: '))
print('Area of Circle is:{}'.format(ob1.area(radius)))
print('Parameter of Circle is:{}'.format(ob1.param(radius)))